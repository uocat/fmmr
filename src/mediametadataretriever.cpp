#include "mediametadataretriever.hpp"
#include "mediametadataretriever_impl.hpp"

namespace fmmr
{
/*static*/
std::shared_ptr<MediaMetadataRetriever> MediaMetadataRetriever::MediaMetadataRetrieverCreate()
{
    MediaMetadataRetriever* ptr = new MediaMetadataRetriever();
    return std::shared_ptr<MediaMetadataRetriever>(ptr);
}

MediaMetadataRetriever::MediaMetadataRetriever()
{
        retriever_.reset(new MediaMetadataRetrieverImpl());
}

MediaMetadataRetriever::~MediaMetadataRetriever()
{
        Destory();
}

int MediaMetadataRetriever::SetDataSource(const char* path)
{
        std::lock_guard<std::mutex> l(lock_);
        return retriever_->SetDataSource(path);
}
int MediaMetadataRetriever::GetFrameAtTime(int64_t time_in_seconds, uint8_t* const* data, int* linesize)
{
        int width = std::atoi(retriever_->ExtractMetadata(VIDEO_WIDTH));
        int height = std::atoi(retriever_->ExtractMetadata(VIDEO_HEIGHT));
        return GetFrameAtTime(time_in_seconds, data, linesize, width, height);
}

int MediaMetadataRetriever::GetFrameAtTime(int64_t time_in_seconds, uint8_t* const* data, int* linesize, int width, int height)
{
        std::lock_guard<std::mutex> l(lock_);
        return retriever_->GetFrameAtTime(time_in_seconds, data, linesize, width, height);
}
const char* MediaMetadataRetriever::ExtractMetadata(const char* key)
{
        std::lock_guard<std::mutex> l(lock_);
        return retriever_->ExtractMetadata(key);
}
void MediaMetadataRetriever::Destory()
{
        std::lock_guard<std::mutex> l(lock_);
        retriever_->Destory(); 
}

}; // namespace fmmr
