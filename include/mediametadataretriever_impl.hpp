#ifndef FFMPEG_MEDIAMETADATARETRIEVER_IMPL_
#define FFMPEG_MEDIAMETADATARETRIEVER_IMPL_

#include "mediametadataretriever.hpp"

#ifdef __cplusplus
extern "C" {
#endif
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
#ifdef __cplusplus
};

#include <vector>
#include <mutex>

#endif //FFMPEG_MEDIAMETADATARETRIEVER_IMPL_

namespace fmmr
{
class MediaMetadataRetriever::MediaMetadataRetrieverImpl
{
public:
    MediaMetadataRetrieverImpl();
    ~MediaMetadataRetrieverImpl();
    int SetDataSource(const char* filepath);
    int GetFrameAtTime(int64_t time_in_seconds, uint8_t* const* data, int* linesize, int width, int height);
    const char* ExtractMetadata(const char* key);
    void Destory();
private:

    void InitDict(AVFormatContext* avformat_context, AVStream* avstream);
    int OpenDecodeCodecContext(int* stream_idx, AVCodecContext** dec_ctx, AVCodec** decoder, AVFormatContext *fmt_ctx, enum AVMediaType type);
    int DecodePacket(AVCodecContext* dec_ctx, const AVPacket* dec_pkt, AVFrame* frame);
    int ScaleFrame(const AVFrame* iframe, 
                          uint8_t* const* dst_data, int* dst_linesize, 
                          int dst_width, int dst_height, enum AVPixelFormat dst_pix_fmt = kTargetPixelFormat);

    int                 video_stream_idx_;
    AVFormatContext*    fmt_ctx_;
    AVCodecContext*     video_dec_ctx_;
    AVStream*           video_stream_;
    AVCodec*            video_codec_;
    int width_;
    int height_;
    enum AVPixelFormat pix_fmt_;

    static const enum AVPixelFormat kTargetPixelFormat = AV_PIX_FMT_RGB24;
    static const enum AVCodecID kTargetCodecID = AV_CODEC_ID_PNG;
};

}; // namespace fmmr
#endif // FFMPEG_MEDIAMETADATARETRIEVER_IMPL_
