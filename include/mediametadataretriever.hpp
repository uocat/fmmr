#ifndef FFMPEG_MEDIAMETADATARETRIEVER_
#define FFMPEG_MEDIAMETADATARETRIEVER_

#include <memory>
#include <vector>
#include <mutex>

namespace fmmr
{

static const char *DURATION = "duration";
static const char *AUDIO_CODEC = "audio_codec";
static const char *VIDEO_CODEC = "video_codec";
static const char *ICY_METADATA = "icy_metadata";
static const char *ROTATE = "rotate";
static const char *FRAMERATE = "framerate";
static const char *CHAPTER_START_TIME = "chapter_start_time";
static const char *CHAPTER_END_TIME = "chapter_end_time";
static const char *CHAPTER_COUNT = "chapter_count";
static const char *FILESIZE = "filesize";
static const char *VIDEO_WIDTH = "video_width";
static const char *VIDEO_HEIGHT = "video_height";

class MediaMetadataRetriever
{
public:
    static std::shared_ptr<MediaMetadataRetriever> MediaMetadataRetrieverCreate();
    ~MediaMetadataRetriever();
    int SetDataSource(const char* filepath);
    int GetFrameAtTime(int64_t time_in_seconds, uint8_t* const* data, int* linesize);
    int GetFrameAtTime(int64_t time_in_seconds, uint8_t* const* data, int* linesize, int width, int height);
    const char* ExtractMetadata(const char* key);
    void Destory();

private:
    MediaMetadataRetriever();

    class MediaMetadataRetrieverImpl;
    std::unique_ptr<MediaMetadataRetrieverImpl> retriever_;
    std::mutex lock_;
};
}; // namespace fmmr
#endif // FFMPEG_MEDIAMETADATARETRIEVER_